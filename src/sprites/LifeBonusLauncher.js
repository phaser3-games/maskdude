import {SpriteLauncher} from './SpriteLauncher'
import {LifeBonus} from './LifeBonus'
class LifeBonusLauncher extends SpriteLauncher{
    constructor(scene, positionArray, group){
        super(scene, positionArray, group);
    }

    createSprite(position){
        let lifeBonus = new LifeBonus(this.scene, position.x, position.y);
        return lifeBonus;
    }

    add2Group(lifeBonus){
        lifeBonus.body.setGravityY(400);
        this.group.add(lifeBonus, true);
        lifeBonus.setVelocityX(-50);
        lifeBonus.body.setBounce(0.5);
        lifeBonus.body.setCircle(20);
    }

    // update(){
    //     for(let position of this.positionArray){
    //         position.x=position.x-1;
    //     }
    //     if(this.positionArray.length>0 && this.positionArray[0].x<400){
    //         let position=this.positionArray[0];
    //         this.positionArray.splice(0, 1);
    //         //launch the rocket
    //         let rock = new Rock(this.scene, position.x, position.y);
    //         rock.body.setGravityY(400);
    //         this.group.add(rock, true);
    //         rock.body.setBounce(1);
    //         rock.body.setCircle(20);
    //         rock.setVelocityX(-200);
    //     }
    // }
}

export {LifeBonusLauncher};