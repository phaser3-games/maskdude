import Phaser from 'phaser'

class Bunny extends Phaser.Physics.Arcade.Sprite {
  constructor (scene, x, y) {
    super(scene, x, y, 'bunny');
    scene.add.existing(this);
    scene.physics.world.enable(this);
    this.body.setGravityY(300);
    this.body.setCollideWorldBounds(true);
    this.direction=-1;
    this.lastGoUpTick=new Date().getTime();
  }

  goLeft(){
    this.setVelocityX(-30);
    this.anims.play('bunny_left', true);
  }

  goRight(){
    this.setVelocityX(30);
    this.anims.play('bunny_right', true);
  }

  update () {
    let currentTick=new Date().getTime();
    if((currentTick-this.lastGoUpTick)>5000){
      if(Math.random()*100<50){
        this.y=this.y-100;
      }
      this.lastGoUpTick=currentTick;
    }
    if(this.direction==-1){
      if(this.x>20){
        this.goLeft();
      }else{
        this.direction=1;
        this.goRight();
      }
    }else{
      if(this.x<550){
        this.goRight();
      }else{
        this.direction=-1;
        this.goLeft();
      }
    }
  }
}

export {Bunny};