import Phaser from 'phaser'

class LifeBonus extends Phaser.Physics.Arcade.Sprite {
  constructor (scene, x, y) {
    super(scene, x, y, 'bonus_life');
    scene.add.existing(this);
    scene.physics.world.enable(this);
  }

  update () {
  }
}

export {LifeBonus};