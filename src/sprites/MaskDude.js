import Phaser from 'phaser'

class MaskDude extends Phaser.Physics.Arcade.Sprite {
  constructor (scene, x, y) {
    super(scene, x, y, 'maskdude');
    scene.add.existing(this);
    scene.physics.world.enable(this);
    this.setGravityY(300);
    this.body.setCollideWorldBounds(true);
    this.superMode=false;
    this.superModeTime=-1;
  }

  setSuperMode(superMode){
    this.superMode=superMode;
    if(this.superMode){
      this.superModeTime=new Date().getTime();
    }
  }

  isSuperMode(){
    return this.superMode;
  }

  goLeft(){
    this.setVelocityX(this.isSuperMode()?-100:-50);
  }

  goRight(){
    this.setVelocityX(this.isSuperMode()?100:50);
  }

  idle(){
    this.setVelocity(0);
  }

  jump(){
    this.setVelocityY(-400);
  }

  update () {
    if(this.superMode){
      let currentTime=new Date().getTime();
      if((currentTime-this.superModeTime)>10000){
        this.setSuperMode(false);
      }
    }
  }
}

export {MaskDude};