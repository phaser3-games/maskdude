import Phaser from 'phaser'

class Coin extends Phaser.Physics.Arcade.Sprite {
  constructor (scene, x, y, superCoin=false) {
    super(scene, x, y, superCoin?'coin_red':'coin');
    this.superCoin=superCoin;
    scene.add.existing(this);
    scene.physics.world.enable(this);
  }

  isSuperCoin(){
    return this.superCoin;
  }

  update () {
   
  }
}

export {Coin};