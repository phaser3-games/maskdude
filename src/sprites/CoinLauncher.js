import {Coin} from './Coin'

class CoinLauncher{
    constructor(scene, group){
        this.scene=scene;
        this.group=group;
    }

    shoot(amount){
        for(let i=0; i<amount; i++){
            let x=Math.random()*600;
            let y=Math.random()*400;
            let coin=new Coin(this.scene, x, y, i==0 || i==10);
            this.group.add(coin);
        }
        this.group.children.iterate((coin) => {
            coin.body.setBounce(0.99);
            let vy = (Math.random() - 0.5) * 2000;
            let vx = (Math.random() - 0.5) * 500;
            coin.setVelocityX(vx);
            coin.setVelocityY(vy);
            coin.body.setCollideWorldBounds(true);
        });
    }
}

export {CoinLauncher};