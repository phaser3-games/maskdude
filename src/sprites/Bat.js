import Phaser from 'phaser'

class Bat extends Phaser.Physics.Arcade.Sprite {
  constructor (scene, x, y) {
    super(scene, x, y, 'bat');
    scene.add.existing(this);
    scene.physics.world.enable(this);
    this.body.setAllowGravity(false);
    this.body.setCollideWorldBounds(true);
    this.firstMove=true;
  }

  update (dude) {
    if(this.firstMove){
      this.firstMove=false;
      this.setVelocityX(-20);
      this.setVelocityY(20);
      this.anims.play('bat_left', true);
    }else{
      if(this.x<dude.x){
        this.setVelocityX(20);
        this.anims.play('bat_right', true);
      }else if(this.x>dude.x){
        this.setVelocityX(-20);
        this.anims.play('bat_left', true);
      }
      if(this.y<dude.y){
        this.setVelocityY(20);
      }else if(this.y>dude.y){
        this.setVelocityY(-20);
      }
    }
  }
}

export {Bat};