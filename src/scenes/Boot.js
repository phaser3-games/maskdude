import Phaser from 'phaser'
import ground_32 from 'images/ground_32.png'
import ground_8 from 'images/ground_8.png'
import spritesheet from 'images/maskdude_spritesheet.png'
import red_spritesheet from 'images/maskdude_red_spritesheet.png'
import bunny_spritesheet from 'images/bunny_spritesheet.png'
import bat_spritesheet from 'images/bat_spritesheet.png'
import maskdude from 'images/maskdude.png'
import maskdude_large from 'images/maskdude_large.png'
import platform from 'images/platform.png'
import bat from 'images/bat.png'
import bunny from 'images/bunny.png'
import coin from 'images/coin.png'
import coin_red from 'images/coin_red.png'
import forest from 'images/forest.png'

class Boot extends Phaser.Scene {
  constructor () {
    super({ key: 'Boot' })
  }

  preload () {
    this.load.image('forest', forest);
    this.load.image('maskdude', maskdude);
    this.load.image('maskdude_large', maskdude_large);
    this.load.image('ground_32', ground_32);
    this.load.image('ground_8', ground_8);
    this.load.image('platform', platform);
    this.load.image('bat', bat);
    this.load.image('bunny', bunny);
    this.load.image('coin', coin);
    this.load.image('coin_red', coin_red);
    this.load.spritesheet('maskdude_anim', 
        spritesheet,
        { frameWidth: 32, frameHeight: 32 }
    );
    this.load.spritesheet('maskdude_red_anim', 
        red_spritesheet,
        { frameWidth: 32, frameHeight: 32 }
    );
    this.load.spritesheet('bunny_anim', 
        bunny_spritesheet,
        { frameWidth: 34, frameHeight: 44 }
    );
    this.load.spritesheet('bat_anim', 
        bat_spritesheet,
        { frameWidth: 46, frameHeight: 30 }
    );
    this.startTime=new Date().getTime();
  }

  create () {
    this.add.image(300, 200, 'forest');
    this.add.image(450, 300, 'maskdude_large');
    this.add.text(250, 200, "Loading......", {fontSize: '18px', fill: 'white'});
    //this.scene.start('Game')
  }

  update(){
    let current=new Date().getTime();
    if(current-this.startTime>5000){
      this.scene.start('Game');
    }
  }
}

export {Boot};