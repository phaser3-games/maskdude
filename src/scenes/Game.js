import Phaser from 'phaser';
import { MaskDude } from '../sprites/MaskDude';
import { Bat } from '../sprites/Bat';
import { Bunny } from '../sprites/Bunny';
import { Coin } from '../sprites/Coin';
import { CoinLauncher } from '../sprites/CoinLauncher';

class Game extends Phaser.Scene {
  constructor() {
    super({ key: 'Game' })
  }

  create() {
    this.platforms = this.physics.add.staticGroup();
    this.platforms.create(300, 400, 'ground_32');
    this.platforms.create(100, 300, 'platform');
    this.platforms.create(400, 300, 'platform');
    this.platforms.create(200, 200, 'platform');
    this.platforms.create(300, 100, 'platform');
    this.coins = this.physics.add.group();
    this.coinLauncher = new CoinLauncher(this, this.coins);
    this.coinLauncher.shoot(25);
 

    this.maskdude = new MaskDude(this, 44, 250);
    this.bat = new Bat(this, 200, 50);
    this.bunny = new Bunny(this, 400, 150);
    this.bunny2 = new Bunny(this, 200, 100);

    this.physics.add.collider(this.maskdude, this.platforms);
    this.physics.add.collider(this.bunny, this.platforms);
    this.physics.add.collider(this.bunny2, this.platforms);
    this.physics.add.collider(this.coins, this.platforms);
    this.physics.add.collider(this.coins, this.coins);
    this.physics.add.overlap(this.maskdude, this.coins, this.collectCoin, null, this);
    this.physics.add.overlap(this.maskdude, this.bunny, this.hit, null, this);
    this.physics.add.overlap(this.maskdude, this.bunny2, this.hit, null, this);
    this.physics.add.overlap(this.maskdude, this.bat, this.hit, null, this);

    this.anims.create({
      key: 'right',
      frames: this.anims.generateFrameNumbers('maskdude_anim', { start: 23, end: 34 }),
      frameRate: 25,
      repeat: -1
    });

    this.anims.create({
      key: 'idle',
      frames: this.anims.generateFrameNumbers('maskdude_anim', { start: 12, end: 22 }),
      frameRate: 25,
      repeat: -1
    });

    this.anims.create({
      key: 'left',
      frames: this.anims.generateFrameNumbers('maskdude_anim', { start: 0, end: 11 }),
      frameRate: 25,
      repeat: -1
    });

    this.anims.create({
      key: 'jump',
      frames: this.anims.generateFrameNumbers('maskdude_anim', { start: 34, end: 40 }),
      frameRate: 25,
      repeat: 3
    });

    this.anims.create({
      key: 'red_right',
      frames: this.anims.generateFrameNumbers('maskdude_red_anim', { start: 23, end: 34 }),
      frameRate: 25,
      repeat: -1
    });

    this.anims.create({
      key: 'red_idle',
      frames: this.anims.generateFrameNumbers('maskdude_red_anim', { start: 12, end: 22 }),
      frameRate: 25,
      repeat: -1
    });

    this.anims.create({
      key: 'red_left',
      frames: this.anims.generateFrameNumbers('maskdude_red_anim', { start: 0, end: 11 }),
      frameRate: 25,
      repeat: -1
    });

    this.anims.create({
      key: 'red_jump',
      frames: this.anims.generateFrameNumbers('maskdude_red_anim', { start: 34, end: 40 }),
      frameRate: 25,
      repeat: 3
    });

    this.anims.create({
      key: 'bunny_right',
      frames: this.anims.generateFrameNumbers('bunny_anim', { start: 20, end: 31 }),
      frameRate: 25,
      repeat: -1
    });

    this.anims.create({
      key: 'bunny_idle',
      frames: this.anims.generateFrameNumbers('bunny_anim', { start: 12, end: 19 }),
      frameRate: 25,
      repeat: -1
    });

    this.anims.create({
      key: 'bunny_left',
      frames: this.anims.generateFrameNumbers('bunny_anim', { start: 0, end: 11 }),
      frameRate: 25,
      repeat: -1
    });

    this.anims.create({
      key: 'bat_left',
      frames: this.anims.generateFrameNumbers('bat_anim', { start: 0, end: 6 }),
      frameRate: 25,
      repeat: -1
    });

    this.anims.create({
      key: 'bat_right',
      frames: this.anims.generateFrameNumbers('bat_anim', { start: 7, end: 13 }),
      frameRate: 25,
      repeat: -1
    });

    this.score=0;
    this.scoreText=this.add.text(480, 10, "Score: "+this.score, {fontSize: '18px', fill: 'yellow'});
    this.cursors = this.input.keyboard.createCursorKeys();
    //this.maskdude.anims.play('right');
  }

  hit(dude, enemy){
    if(!dude.isSuperMode()){
      dude.disableBody(true, true);
    }
  }

  collectCoin(dude, coin){
    this.score++;
    coin.disableBody(true, true);
    this.coins.children.delete(coin);
    if(coin.isSuperCoin()){
      dude.setSuperMode(true);
    }
    if(this.coins.children.size<5){
      this.coinLauncher.shoot(25);
    }
  }

  update() {
    this.scoreText.setText("Score: "+this.score);
    if (this.cursors.right.isDown) {
      this.maskdude.goRight();
      this.maskdude.anims.play(this.maskdude.isSuperMode()?'red_right':'right', true);
    } else if (this.cursors.left.isDown) {
      this.maskdude.goLeft();
      this.maskdude.anims.play(this.maskdude.isSuperMode()?'red_left':'left', true);
    } else if(this.maskdude.body.touching.down){
      this.maskdude.idle();
      this.maskdude.anims.play(this.maskdude.isSuperMode()?'red_idle':'idle', true);
    }

    if (this.cursors.up.isDown && this.maskdude.body.touching.down) {
      this.maskdude.jump();
      this.maskdude.anims.play(this.maskdude.isSuperMode()?'red_jump':'jump', true);
    }

    this.maskdude.update();
    this.bunny.update();
    this.bunny2.update();
    this.bat.update(this.maskdude);
  }
}

export { Game };
